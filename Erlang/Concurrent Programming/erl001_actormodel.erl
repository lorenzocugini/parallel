-module(erl001_actormodel).
-export([go/0, loop/0]).

go() ->
	Pid2 = spawn(erl001_actormodel, loop, []),		% spawn a process from module erl..., with function loop and w/out arguments
	Pid2 ! {self(), hello},							% send a message (!) to process with Pid self() (my pid), saying hello (hello is an atom)
	receive
		{Pid2, Msg} ->				% Take one message from Pid2 from my mailbox/msg queue
			io:fwrite("P1 ~w~n", [Msg])
	end,
	%Send to Pid2 a message telling him to stop. The message contains only the atom stop.
	Pid2 ! stop.

% IMPORTANT. 	Prints are not discriminated. It just prints the output on cmd. Remember to print something to distinguish between who is printing.

loop() ->
	receive
		{From, Msg} ->
			From ! {self(), Msg},	% Echoes the received message. Sends the Msg to From
			io:fwrite("Marco Columbro giovane amatore~n"),
			loop();
		stop ->		% If it receives a message containing only the atom hello, 
			void	% Just do an atom. It won't do anything, it's just a placeholder to avoid recursive calls.
					% If I call loop(); at the end again, the process won't stop!
	end.