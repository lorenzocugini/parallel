-module(erl002_pingpong).
-export([start/0, ping/2, pong/0]).

-export([start2/0]).

start() ->	% Basically the main
	Pong_Pid = spawn(erl002_pingpong, pong, []),				%Start the pong
	spawn(erl002_pingpong, ping, [2, Pong_Pid]).	%Start the ping, it passes the number of exchanges and the pid of pong to exchange messages
	%This will print to something like <0.36.0>, that's the return value of start()

ping(0, Pong_Pid) ->
	Pong_Pid ! finished,
	io:fwrite("Last ping!~n");

ping(N, Pong_Pid) ->
	Pong_Pid ! {ping, self()},	%Send a ping, wait for a pong
	receive
		pong ->
			io:fwrite("PING!~n")
	end,
	ping(N-1, Pong_Pid).	%Recursive function

pong() ->
	receive						%Receive a ping, send a pong
		{ping, Ping_Pid} ->
			io:fwrite("PONG!~n"),
			Ping_Pid ! pong,
			pong();

		finished ->
			io:fwrite("Last pong!~n")
	end.

%	To avoid the explicit use of Pids, we can register a process.
%start2 () ->
	%register(pong, spawn(erl002_pingpong, pong, [])),
	%spawn(erl002_pingpong, ping, [2]).	% Already knows Pong_Pid!
	%Ping can now send messages directly to pong! (the registered alias is pong)
	%pong ! {ping, self()}