% The idea is to build a server and some clients connected to it.
% Clients send and message, the server responds with an ack and the client acks too.
-module(erl003_clientserver).
-export( [server/0, client/2, start/1] ).

% Function of the server
server() ->
	receive
		% When it receives a message, it acks it.
		{From, ID} ->
			From ! {server_adr, ack},
			io:fwrite("S - Acked ~.10B~n", [ID]),
			server()	% then restarts to wait for another message.
	end.


% The client sends a message to the server, one for each N.
client(ID, 0) ->
	io:fwrite("------------- End of ~.10B~n~n", [ID]);

client(ID, N) when N > 0 ->
	server_adr ! {self(), ID},
	io:fwrite("C - Send to server~n"),
	receive
		{server_adr, ack} ->
			% How fun, Erlang. We print in base 10 an integer.
			io:fwrite("C - ~.10B received an ack!~n", [ID]),
			client(ID, N-1)
	end.


% Register a server and a fixed number of clients. Client's registration
% cannot be done inside of a loop since the aliases must be different!
start(NumOfClients) ->
	register(server_adr, spawn(erl003_clientserver, server, [])),
	register(client_01, spawn(erl003_clientserver, client, [1, 5])),
	register(client_02, spawn(erl003_clientserver, client, [2, 5])).