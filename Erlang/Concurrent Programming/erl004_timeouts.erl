-module(erl004_timeouts).
-export([start/0, client/0, server/0]).


client() ->
	addrS ! {self(), "Did Ross cheat on Rachel?~n"}.

server() ->
	receive
		{_, _} ->
			io:fwrite("No, they were on a break!~n")
	after
		1000 ->
			io:fwrite("But still they end up together.~n")

	% The 'after' expression is not something that happens after T milliseconds.
	% If for that amount of time no pattern is returned, then the after body is 
	% evaluated.

	end.

start() ->
	register(addrS, spawn(erl004_timeouts, server, [])),
	register(addrC, spawn(erl004_timeouts, client, [])).


% By doing nothing in receive and something in after, we slept for T milliseconds.
sleep(T) ->
	receive
	after
		T -> true
	end.