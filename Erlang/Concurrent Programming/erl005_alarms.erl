-module(erl005_alarms).
-export([alarm/3, set_alarm/2, start/0]).

% spawn(Function) is the standard method to spawn a process.
% spawn(module_name, func_name, arguments) is an example

% Sleep for T and then send an Alarm message
alarm(Pid, T, Alarm) ->
	receive
	after
		T ->
			Pid ! Alarm
	end.

set_alarm(T, What) ->
	spawn(erl005_alarms, alarm, [self(), T, What]).

start() ->
	set_alarm(3000, "Reach the stars, fly a fantasyyyyyyyyyyyyy~n~n").