% A link is created between two processes. When two processes are linked, when one dies or
% terminates, the other is killed too.

-module(erl006_links).
-export([start/0]).
% Hey you, now hear this! IF YOU DO NOT EXPORT the functions that you spawn,
% the spawn function won't work! Haha, you got me here Erlang, give me back
% one hour of my F*****G time!
-export([master/0, slave/0]).

slave() ->
	receive
		youaretoofat ->
			io:fwrite("HOW DARE YOU!~n"),
			master ! {"Farewell, you monster!~n"}
	end.

master() ->
	% Setting trap_exit to true means that we can catch EXIT messages!
	process_flag(trap_exit, true),
	
	Child = spawn_link(erl006_links, slave, []),
	io:fwrite("Slave created~n"),
	Child ! youaretoofat,
	
	master_loop().

master_loop() ->
	receive
		{Msg} ->
			io:fwrite("Slave says: ~p~n.Fatty is dead~n", [Msg]),
			master_loop();
		{'EXIT', _, _} ->	% 'EXIT' is the default message sent when a process dies
			io:fwrite("Child dying~n")
		  %	master_loop()	% You can call this how many times you want, when a linked process dies, we die too.
							% If we call it though, the registered 'master' already died, so we will met an exception error: bad argument
	end.

start() ->
	register(master, spawn(erl006_links, master, [])).