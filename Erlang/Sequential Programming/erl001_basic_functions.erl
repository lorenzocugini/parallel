-module(erl001_basic_functions).
-export([factorial/1 , test_if/2]).

factorial(0) -> 1;
factorial(N) -> N * factorial(N-1).

% After installing the BEAM virtual machine, compile the file with
% > erlc basic_functions.erl

% When you use BEAM, you MUST remember the dots '.'.
% After loading BEAM, the compilation of a file happens with c(module_name). (NOTE THE DOT!)
% If the compilation is successfull, it will yield {ok, module_name}
% Using a function of a compiled module, we need to call it by module:function(params). (NOTE THE DOT!!)

% Formatting needs the usual \n and family. Here it's ~ (AltGr + ì)
% When calling a function with multiple parameters with		> erl001_basic_functions:test_if(1, 1). (NOTE THE DOT!!!)
test_if(A, B) ->
	if
		A == 5 ->
			io:format("A == 5~n", []);		% ~n is the same of newline or \n
			% NB: it's exactly like an if-else. Matching the first pattern marks the termination of the if!
		B == 6 ->
			io:format("B == ~p~n", [B]);	% ~p prints any value. The value to print needs to be contained in the parameter list []
		true ->
			io:format("Else branch met.~n")	% Last if call MUST NOT have either ; or the . (Fuck you, Erlang)
	end.