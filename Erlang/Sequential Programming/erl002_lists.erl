-module(erl002_lists).

% AllList = [1,2,3,4,5].
% Cannot do this. We need everything to be inside a function.


-export([start/0]).
start() ->
	FullList = [1,2,3,4,5],
	[First | Rest] = [1,2,3,4,5],
	io:fwrite("~p~n", [First]),		%Correct function to print an output.
	io:fwrite("~w~n", [Rest]).