-module(erl003_loops).

%We have while loops available? Obviously not.
%But we have recursion!

-export([while/2]).
-export([whiwhile/1]).
-export([for/2]).

while( [], Acc ) -> Acc;
while( [_|T], Acc) ->
	io:fwrite("~w~n", [Acc]),
	while(T, Acc+1).
%This function basically counts the elements in a list.

% -export([whiwhile/2]). Can't do this here. Exports must be before any function definition.
% whiwhile([]) -> []; By removing this, we will end up in an exception since the empty pattern doesn't match with any clause.
% But with that, we will return the empty list [], which will be printed.
whiwhile([]) ->
	io:fwrite("",[]);				% Not a good practice (there is surely a better way), but if a path is useless we should do nothing.
whiwhile([F|T]) ->
	io:fwrite("~w~n", [F] ),		% To print a list the square brackets are needed!
	whiwhile(T).



%We also can create a for loop
for(0, _) -> [];
for(N, Term) when N > 0 ->		% Term is not a keyword, but an ATOM. It just refers to the generic variable that may be passed.
	io:fwrite("Hello~n"),		% With fwrite we can avoid the brackets with the variables if none is needed.
	for(N-1, Term).

%That's a descending for, so it basically coincides with Satan. Using guards we can also count upwards:
% for(0, _) -> io:fwrite("");
% for(N, Term) when ..?
% when N <= (?), we don't know this at compile-time! It's a value passed by the caller. We cannot do upcounting fors.