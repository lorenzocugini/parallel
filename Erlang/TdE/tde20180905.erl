% Define a function create_pipe, which takes a list of names and creates a process of each element of the list,
% each process registered as its name in the list; e.g. with [one, two], it creates two processes called âoneâ and âtwoâ.

% The processes are âconnectedâ (like in a list, there is the concept of ânext processâ) from the last to the first, e.g. with [one, two, three],
% the process structure is the following:
%	three â two â one â self,
% this means that the next process of âthreeâ is âtwoâ, and so on;

% self is the process that called create_pipe.

% Each process is a simple repeater,
% showing on the screen its name and the received message,
% then sends it to the next process.
% Each process ends after receiving the âkillâ message, unregistering itself




% Take a list of names and register one process for each name.
%
%create_pipe( [X|XS] ) ->
%	Proc = spawn( ?MODULE, _, _ ),
%	register(X, Proc)
%	create_pipe(XS);
%
%create_pipe( [] ) -> End.

% Processes are connected. We keep a reference to the previous process (Next is X), and we pass it to the new process.
%
%create_pipe( [X|XS], Next ) ->
%	P = spawn( ?MODULE, _, [Next, X]), where we've passed the name too to let the process know is name (may be useful)
%	register(X,P),
%	create_pipe(XS,X).

% Each process is a simple repeater, args are next_proc and current_proc_name
% Repeater function
%
%repeater(Next, Name) ->
%	receive
%		Msg ->
%			io:fwrite("I am ~p, Msg: ~p~n", [Name, Msg]),
%			Next ! Msg
%	end.

% Each process dies after receiving the kill message, unregistering itself
repeater(Next, Name) ->
	receive
		Msg ->
			io:fwrite("I am ~p, Msg: ~p~n", [Name, Msg]),
			Next ! Msg,
			repeater(Next, Name);
		kill ->
			unregister(Name),
			Next ! kill	  % Not needed. Indeed this is a repeater, but nothing explictly says that it should repeat the kill too.
	end.

create_pipe([], End) -> End;
create_pipe([X|Xs], Next) ->
    P = spawn(?MODULE, repeater, [Next, X]),
    register(X, P),
    create_pipe(Xs, X).